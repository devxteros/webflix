<?php
/*
Plugin Name: Webflix Peliculas
Plugin URL:
Description: Gestión de Peliculas WebFlix
Version: 1.0
Author: Juan Manuel Lora
Author URI: https://bitbucket.org/devxteros/webflix/src/master/
*/

function crear_post_type_peliculas(){


  $labels = array(
      'name'                  => _x( 'Peliculas', 'Post type general name', 'default'),
      'singular_name'         => _x( 'Pelicula', 'Post type singular name', 'default'),
      'menu_name'             => _x( 'Peliculas', 'Admin Menu text', 'default'),
      'parent_item_colon'     => __( 'Pelicula Padre:', 'default'),
      'all_items'             => __( 'Ver Peliculas', 'default'),
      'view_item'             => __( 'Ver Pelicula', 'default'),
      'add_new'               => __( 'Crear Pelicula', 'default'),
      'add_new_item'          => __( 'Nueva Pelicula', 'default'),
      'edit_item'             => __( 'Editar Pelicula', 'default'),
      'update_item'           => __( 'Editar Pelicula', 'default'),
      'search_items'          => __( 'Buscar Pelicula', 'default'),
      'not_found'             => __( 'No se encontraron peliculas.', 'default'),
      'not_found_in_trash'    => __( 'No hay peliculas en Papelera', 'default'),
      'name_admin_bar'        => _x( 'Pelicula', 'Add New on Toolbar', 'default' ),

      /*

      'new_item'              => __( 'Nueva Categoria', 'textdomain' ),
      'featured_image'        => _x( 'Book Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
      'archives'              => _x( 'Book archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
      'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
      'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
      'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
      'items_list_navigation' => _x( 'Books list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
      'items_list'            => _x( 'Books list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
      */
  );

  $args = array(
      'labels'             => $labels,
      'descriptions'       =>_x( 'Peliculas', 'Post type singular name'),
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'pelicula' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'menu_icon'          => 'dashicons-tickets-alt',
      'can_export'         => true,
      'exclude_from_search' => false,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions', 'custom-fields' ),
  );


  register_post_type( 'pelicula', $args );

}


add_action( 'init', 'crear_post_type_peliculas');



function taxonomia_categoria_pelicula(){
  register_taxonomy( 'categoria_pelicula', 'pelicula',
          array(
            'label'=>__('Categoria'),
            'rewrite'=>array('slug'=>'categoria'),
            'hierarchical'=>true,
            'show_ui' => true,
            'show_in_nav_menus'=>true,
            'show_admin_column'=>true,
            'query_var' => true,
            'public' => true,
            //'exclude_from_search' => false,
          )
  );
}

add_action( 'init', 'taxonomia_categoria_pelicula');


function taxonomia_actores_principales(){
  register_taxonomy( 'actores_principales', 'pelicula',
          array(
            'label'=>__('Actores Principales'),
            'rewrite'=>array('slug'=>'actores-principales'),
            'hierarchical'=>false,
            'show_ui' => true,
            'show_in_nav_menus'=>false,
            'show_admin_column'=>true,
            'query_var' => true,
            'public' => true,
            //'exclude_from_search' => false,
          )
  );


}

add_action( 'init', 'taxonomia_actores_principales');




function taxonomia_paises(){
  register_taxonomy( 'paises', 'pelicula',
          array(
            'label'=>__('Paises'),
            'rewrite'=>array('slug'=>'paises'),
            'hierarchical'=>false,
            'show_ui' => true,
            'show_in_nav_menus'=>false,
            'show_admin_column'=>true,
            'query_var' => true,
            'public' => true,
            //'exclude_from_search' => false,
          )
  );


}

add_action( 'init', 'taxonomia_paises');





function themename_add_post_thumbnail_column( $cols ) {

	$cols_start = array_slice( $cols, 0, 2, true );
	$cols_end   = array_slice( $cols, 2, null, true );
	$custom_cols = array_merge(
		$cols_start,
		array( 'themename_post_thumb' => __( 'Imagen', 'themename' ) ),
		$cols_end
	);
	return $custom_cols;
}

add_filter( 'manage_posts_columns', 'themename_add_post_thumbnail_column', 5 ); // agrega imagen al post

function themename_display_post_thumbnail_column( $col, $id ) { // render de la imagen
	switch( $col ){
		case 'themename_post_thumb':
			if( function_exists( 'the_post_thumbnail' ) ) {
				echo the_post_thumbnail( 'thumbnail' );
			} else {
				echo __( 'Not supported in theme', 'themename' );
			}
			break;
	}
}
add_action( 'manage_posts_custom_column', 'themename_display_post_thumbnail_column', 5, 2 );
