<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'webflix_db');

/** MySQL database username */
define('DB_USER', 'webflix_user');

/** MySQL database password */
define('DB_PASSWORD', 'webflix_pass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FTP_HOST', 'localhost');
define('FTP_USER', 'daemon');
define('FTP_PASS', 'xampp');



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D4x+@;F0(C_.tQ!Rf; Xs!@Ht!d*~[9Wtp(oUe%ii|I(~l&U5sdrL[B78ZS!Q6,5');
define('SECURE_AUTH_KEY',  'c8_.3ky]W}yHi{g*!I+<a>(kR7:s5[58s7vI%E{#5hjp1 &:H61h(b7^ni0(p@P4');
define('LOGGED_IN_KEY',    'f[u9Z>&g(S33`3(tRM;UNU&*p~f>h5Ctto=Y6@PD4(Pg:ig`zxvZu=9]tb!7^}i2');
define('NONCE_KEY',        'yvzf!3:FBMDt3,r{mY%(w&Ssh!|h07[`pG$0*_;Bt5rq0gX_CPA[-*329LSkq7Oo');
define('AUTH_SALT',        'bFPcZpX:~sM5%[]}W.tm]pI]}4 LiD!/CPOI[* 7ayQN-__ZiR6Qm^Boyplu3{U@');
define('SECURE_AUTH_SALT', 'Gk4*HL%tcdiF*-H7wPYmS@e]rX&*VPfy6s?>GnebGOa]$s -xP|>t0IR]Os XDq[');
define('LOGGED_IN_SALT',   'KnLiA%IJi2mdu1tl#9,^MJWn6xwk}?0j[aV^828(mJ}T,=Tt.|==l+B>Y*:ie6hA');
define('NONCE_SALT',       '|v)jYhAuH$3.8c4rSgu9-m@!>9]UKUwes|;6c-U7>}3qH:oZApVQ<B$Ek0IZh3?)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
